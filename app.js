const express = require("express")
const path = require("path")

const app = express()
const publicFolderPath = path.join(__dirname, "public")

app.use(express.json())
app.use(express.static(publicFolderPath))

let users = []

// add POST request listener here
app.post('/api/user', function (req, res) {

    let taken = false;

    for (user of users) {
        if (user.username === req.body.username) {
            taken = true;
            break
        } else {
            taken = false
        }
    }

    if (!taken) {
        req.body.id = Math.random()
        users.push(req.body)
        console.log(req.body)
        res.status(201)
        res.send(req.body)
    } else {
        res.status(409)
        throw new Error("USERNAME TAKEN")
        // res.send(error)
    }
})

app.get('/users', (req, res) => {
    res.statusCode = 200;
    // res.setHeader('Content-Type', 'application/javascript');
    const responseBody = users
    res.send(responseBody);
})

app.listen(3000, () => console.log(`listening`));