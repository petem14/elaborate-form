const userCreateForm = document.getElementById("user-create-form")
const userCreateSubmitButton = userCreateForm.querySelector("button[type='submit']")

userCreateSubmitButton.addEventListener('click', function submitHandler() {
    event.preventDefault()

    let userInput = document.getElementsByTagName('input')
    let inputValues = []

    for (let i = 0; i < userInput.length; i++) {
        inputValues.push(userInput[i].value)
        console.log(userInput[i])
    }

    let radios = document.getElementsByName('comm')
    let commChoice;
    for (let radio of radios) {
        if (radio.checked) {
            commChoice = radio.value
        }
    }

    let checkBoxes = document.getElementsByClassName('device-check');
    let checkChoices = []
    for (let checkBox of checkBoxes) {
        if (checkBox.checked) {
            checkChoices.push(checkBox.value)
        }
    }
    let selectOptions = document.getElementById('selection').options
    let selectedIndex = selectOptions.selectedIndex
    let selectedTitle;
    console.log(selectedIndex)
    if (selectedIndex != -1) {
        selectedTitle = selectOptions[selectedIndex].value
    } else {
        selectedTitle = 'none'
    }
    // console.log(radios[0].value)

    let sliderVal = document.getElementById('slider').value

    const newUser = new User(inputValues, commChoice, checkChoices, selectedTitle, sliderVal)


    // //stack overflow fetch post example
    fetch("/api/user", {
        method: "post",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },

        //make sure to serialize your JSON body
        body: JSON.stringify(newUser)
    }).then((response) => {
        if (response.status === 201) {
            // let text = document.createTextNode('success')
            // document.body.appendChild(text)
            alert('success')
            // console.log(response)
            return response.json()
        } else {
            // let text2 = document.createTextNode('username taken')
            // document.body.appendChild(text2)
            alert('Username already taken')
        }
    }).then((data) => {
        if (data) {
            console.log(data)
        }
    })

})

class User {
    constructor(array, commChoice, checkChoices, selectedTitle, sliderVal) {
        this.username = array[0]
        this.email = array[1]
        this.birthdate = array[2]
        this.color = array[3]
        this.phone = array[4]
        this.media = array[5]
        this.comm = commChoice
        this.devices = checkChoices
        this.title = selectedTitle
        this.slider = sliderVal
    }
}